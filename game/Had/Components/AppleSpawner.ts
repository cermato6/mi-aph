import { ATTR_MODEL, ATTR_FACTORY, MSG_APPLE_EATEN, TileType } from '../Constants';
import Component from '../../../ts/engine/Component';
import { Model } from '../Model';
import Msg from '../../../ts/engine/Msg';

/**
 * Component that puts apples into game
 */
export class AppleSpawner extends Component {
    private model: Model;

    onInit() {
        this.subscribe(MSG_APPLE_EATEN);
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_APPLE_EATEN) {
            let position = this.model.getRandomEmptyTile();
            if (position) {
                this.model.appleSprite.x = position.x;
                this.model.appleSprite.y = position.y;
                this.model.apple.x = position.x;
                this.model.apple.y = position.y;
                this.model.addTile(position, TileType.Apple);
            }
            else {
                this.model.appleSprite.remove();
            }
        }
    }
}