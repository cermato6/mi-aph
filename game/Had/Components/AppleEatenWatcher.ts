import { Model } from '../Model';
import { ATTR_MODEL, MSG_APPLE_COLLISON, MSG_APPLE_EATEN, MSG_GAMESPEED } from '../Constants';
import Component from '../../../ts/engine/Component';
import Msg from '../../../ts/engine/Msg';

/**
 * Watcher for apple eaten
 */
export class AppleEatenWatcher extends Component {

    private model: Model;

    onInit() {
        this.subscribe(MSG_APPLE_COLLISON);
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_APPLE_COLLISON) {
            this.model.currentScore++;
            this.sendMessage(MSG_APPLE_EATEN);
            this.sendMessage(MSG_GAMESPEED);
        }
    }
}