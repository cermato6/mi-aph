import { GenericComponent } from '../../../ts/components/GenericComponent';
import { SOUND_INTRO, MSG_GAME_STARTED, SOUND_DEATH, SOUND_EAT, MSG_APPLE_EATEN, MSG_LIFE_LOST, SOUND_GAMEOVER, MSG_GAME_COMPLETED, MSG_COMMAND_RESPAWN } from '../Constants';

/**
 * Component that plays all sounds based on emitted events
 */
export class SoundComponent extends GenericComponent {

    constructor(){
        super(SoundComponent.name);
        
        // using generic component is much simpler
        this.doOnMessage(MSG_GAME_STARTED, (cmp, msg) => (<any>PIXI.loader.resources[SOUND_INTRO]).sound.play());
        this.doOnMessage(MSG_APPLE_EATEN, (cmp, msg) => (<any>PIXI.loader.resources[SOUND_EAT]).sound.play())
        this.doOnMessage(MSG_COMMAND_RESPAWN, (cmp, msg) => (<any>PIXI.loader.resources[SOUND_DEATH]).sound.play())
        this.doOnMessage(MSG_GAME_COMPLETED, (cmp, msg) => (<any>PIXI.loader.resources[SOUND_GAMEOVER]).sound.play())
    }
}