// tags for game objects
export const TAG_INTRO = "intro";
export const TAG_BACKGROUND = "background";

//snake
export const TAG_SNAKE_UP_DOWN = "up_down";
export const TAG_SNAKE_UP_LEFT = "up_left";
export const TAG_SNAKE_UP_RIGHT = "up_right";
export const TAG_SNAKE_DOWN_LEFT = "down_left";
export const TAG_SNAKE_DOWN_RIGHT = "down_right";
export const TAG_SNAKE_LEFT_RIGHT = "left_right";

export const TAG_SNAKE = "snake";
export const TAG_WALL = "wall";
export const TAG_APPLE = "apple";

export const TAG_HEAD_UP = "head_up";
export const TAG_HEAD_LEFT = "head_left";
export const TAG_HEAD_DOWN = "head_down";
export const TAG_HEAD_RIGHT = "head_right";

export const TAG_TAIL_UP = "tail_up";
export const TAG_TAIL_LEFT = "tail_left";
export const TAG_TAIL_DOWN = "tail_down";
export const TAG_TAIL_RIGHT = "tail_right";

export const TAG_SCORE = "score";
export const TAG_LIFE = "life";
export const TAG_STATUS = "status";

export enum DIRECTION {
    Up = 0,
    Down,
    Left,
    Right,
}

//snake must be last
export enum TileType {
    None = 0,
    Wall,
    Apple,
    Snake
}

export enum HitType {
    Wall = 0,
    Apple,
    Snake
}


// message keys
export const MSG_GAME_STARTED = "game_start";
export const MSG_GAME_COMPLETED = "game_completed";
export const MSG_LEVEL_COMPLETED = "level_completed";

export const MSG_COMMAND_NEWGAME = "new_game";
export const MSG_COMMAND_FINISH_LEVEL = "level_finish";
export const MSG_COMMAND_RESPAWN = "level_respawn";

export const MSG_OBJECT_HIT = "object_hit";
export const MSG_APPLE_EATEN = "apple_eaten";
export const MSG_LIFE_LOST = "life_lost";
export const MSG_GAMESPEED = "gamespeed";

export const MSG_APPLE_COLLISON = "apple_collision";
export const MSG_LIFELOST_COLLISION = "lifelost_collision"

// sound aliases
export const SOUND_INTRO = "s_intro";
export const SOUND_GAMEOVER = "s_gameover";
export const SOUND_EAT = "s_eat";
export const SOUND_DEATH = "s_death";

// attribute keys
export const ATTR_MODEL = "MODEL";
export const ATTR_FACTORY = "FACTORY";

// alias for texture
export const TEXTURE_HAD = "had";
export const TEXTURE_BACKGROUND = "background";
export const TEXTURE_INTRO = "intro";

// height of the scene will be set to 24 units for the purpose of better calculations
export const SCENE_HEIGHT = 24;

// native height of the game canvas. If bigger, it will be resized accordingly
export const SPRITES_RESOLUTION_HEIGHT = 768;

// alias for config file
export const DATA_JSON = "DATA_JSON";


