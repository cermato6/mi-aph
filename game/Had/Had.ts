import { PixiRunner } from '../../ts/PixiRunner'
import { TEXTURE_HAD, SPRITES_RESOLUTION_HEIGHT, SCENE_HEIGHT, DATA_JSON, TEXTURE_BACKGROUND, SOUND_INTRO, SOUND_GAMEOVER, SOUND_EAT, SOUND_DEATH, TEXTURE_INTRO, MSG_COMMAND_NEWGAME } from './Constants';
import { Factory } from './Factory';
import { Model } from './Model';


class Had {
    engine: PixiRunner;

    // Start a new game
    constructor() {
        this.engine = new PixiRunner();
        
        let canvas = (document.getElementById("gameCanvas") as HTMLCanvasElement);

        let screenHeight = canvas.height;
        
        // calculate ratio between intended resolution (here 600px of height) and real resolution
        // - this will set appropriate scale 
        let gameScale = SPRITES_RESOLUTION_HEIGHT / screenHeight;
        // scale the scene to 10 units if height
        let resolution = screenHeight / SCENE_HEIGHT * gameScale;
        this.engine.init(canvas, resolution / gameScale);
        // set global scale which has to be applied for ALL sprites as it will
        // scale them to defined unit size
        Factory.globalScale = 1 / resolution;

        PIXI.loader
            .reset()    // necessary for hot reload
            .add(TEXTURE_HAD, 'static/had/sprites.png')
            .add(TEXTURE_BACKGROUND, 'static/had/background.png')
            .add(TEXTURE_INTRO, 'static/had/intro.png')
            .add(SOUND_INTRO, 'static/had/intro.mp3')
            .add(SOUND_GAMEOVER, 'static/had/gameover.wav')
            .add(SOUND_EAT, 'static/had/appleeaten.mp3')
            .add(SOUND_DEATH, 'static/had/death.mp3')
            .add(DATA_JSON, 'static/had/data.json')
            .load(() => this.onAssetsLoaded());
    }

    onAssetsLoaded() {
        // init factory and model
        let factory = new Factory();
        let model = new Model();
        model.loadModel(PIXI.loader.resources[DATA_JSON].data);
        factory.resetGame(this.engine.scene, model, MSG_COMMAND_NEWGAME);
    }
}

export var had = new Had();

