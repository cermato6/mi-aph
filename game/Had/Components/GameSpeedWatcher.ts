import { Model } from '../Model';
import { ATTR_MODEL, MSG_GAMESPEED } from '../Constants';
import Component from '../../../ts/engine/Component';
import Msg from '../../../ts/engine/Msg';

/**
 * Watcher for game speed
 */
export class GameSpeedWatcher extends Component {

    private model: Model;

    onInit() {
        this.subscribe(MSG_GAMESPEED);
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_GAMESPEED) {
            this.model.gameSpeedMultiplier += 0.1;
        }
    }
}