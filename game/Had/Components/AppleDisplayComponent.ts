import Component from "../../../ts/engine/Component";
import { Model } from "../Model";
import { MSG_APPLE_EATEN, ATTR_MODEL } from "../Constants";
import Msg from "../../../ts/engine/Msg";
import { PIXICmp } from "../../../ts/engine/PIXIObject";

/**
 * Display for score
 */
export class AppleDisplayComponent extends Component {
    private model: Model;

    onInit() {
        this.subscribe(MSG_APPLE_EATEN);
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_APPLE_EATEN) {
            (<PIXICmp.Text>this.owner).text = `SCORE: ${this.model.currentScore}`;
        }
    }
}