import { Model } from '../Model';
import { ATTR_MODEL, MSG_LEVEL_COMPLETED, MSG_GAME_COMPLETED, MSG_COMMAND_RESPAWN } from '../Constants';
import Component from '../../../ts/engine/Component';
import Msg from '../../../ts/engine/Msg';
import { PIXICmp } from '../../../ts/engine/PIXIObject';
import { GenericComponent } from '../../../ts/components/GenericComponent';


export class StatusComponent extends GenericComponent {
    private model: Model;

    constructor() {
        // use GenericComponent to pile up all message handlers
        super(StatusComponent.name);
        this.doOnMessage(MSG_COMMAND_RESPAWN, (cmp, msg) => this.showText(`RESPAWNING`));
        this.doOnMessage(MSG_LEVEL_COMPLETED, (cmp, msg) => this.showText(`LEVEL COMPLETED`));
        this.doOnMessage(MSG_GAME_COMPLETED, (cmp, msg) => this.showText(`TOTAL SCORE: ${this.model.currentScore}`, 8000));
    }

    onInit() {
        super.onInit();
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }

    protected showText(text: string, delay: number = 1000) {
        let textObj = <PIXICmp.Text>this.owner;
        textObj.text = text;
        textObj.visible = true;

        this.scene.invokeWithDelay(delay, () => {
            textObj.visible = false;
        });
    }
}