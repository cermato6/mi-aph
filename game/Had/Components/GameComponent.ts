import { Factory } from '../Factory';
import Component from '../../../ts/engine/Component';
import { Model } from '../Model';
import Msg from '../../../ts/engine/Msg';
import { ATTR_FACTORY, ATTR_MODEL, MSG_COMMAND_FINISH_LEVEL, MSG_LIFE_LOST, MSG_COMMAND_RESPAWN, MSG_GAME_COMPLETED, MSG_LEVEL_COMPLETED, MSG_COMMAND_NEWGAME } from '../Constants';


/**
 * Component that orchestrates main logic of the game
 */
export class GameComponent extends Component {
    private model: Model;
    private factory: Factory;

    onInit() {
        this.subscribe(MSG_COMMAND_FINISH_LEVEL, MSG_COMMAND_RESPAWN);

        this.factory = this.scene.getGlobalAttribute(ATTR_FACTORY);
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_COMMAND_FINISH_LEVEL) {
            this.finishLevel();
        }
        else if (msg.action == MSG_COMMAND_RESPAWN) {
            this.RespawnSnake();
        }
    }

    protected finishLevel() {
        // go to the next level
        if (this.model.currentLevel == this.model.maxLevel) {
            this.model.currentLevel = 0; // back to intro scene
            this.sendMessage(MSG_GAME_COMPLETED);
            this.reset(8500, MSG_COMMAND_NEWGAME);
        } else {
            this.model.currentLevel++;
            this.sendMessage(MSG_LEVEL_COMPLETED);
            this.reset(1500, MSG_COMMAND_FINISH_LEVEL);
        }
    }

    protected RespawnSnake() {
        this.reset(1500, MSG_COMMAND_RESPAWN);
    }

    private reset(delay: number, cmd: string) {
        this.model.gameSpeedMultiplier = 0;

        this.scene.invokeWithDelay(delay, () => this.factory.resetGame(this.scene, this.model, cmd));
    }
}