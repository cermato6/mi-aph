import Component from "../../../ts/engine/Component";
import { Model, COLUMNS_NUM, GAME_OFFSET, ROWS_NUM } from "../Model";
import { ATTR_MODEL, DIRECTION, ATTR_FACTORY, TAG_HEAD_DOWN, TAG_HEAD_RIGHT, TAG_HEAD_LEFT, TAG_HEAD_UP, TAG_SNAKE_UP_DOWN, TAG_TAIL_DOWN, TAG_TAIL_RIGHT, TAG_TAIL_LEFT, TAG_TAIL_UP, TAG_SNAKE, TAG_SNAKE_DOWN_RIGHT, TAG_SNAKE_LEFT_RIGHT, TAG_SNAKE_UP_RIGHT, TAG_SNAKE_DOWN_LEFT, TAG_SNAKE_UP_LEFT, TileType, MSG_OBJECT_HIT, HitType } from "../Constants";
import Vec2 from "../../../ts/utils/Vec2";
import { Factory } from "../Factory";
import { KeyInputComponent, KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, KEY_SPACEBAR } from "../../../ts/components/KeyInputComponent";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import PIXIObjectBuilder from "../../../ts/engine/PIXIObjectBuilder";
import { PathFinderContext } from "../../../ts/utils/Pathfinding";
import { BreadthFirstSearch } from "../BreadthFirstSearch";

/**
 * Component that handles snake movement
 */
export class SnakeController extends Component {
    protected model: Model;
    protected factory: Factory;

    protected timePassed: number;
    protected headTextures: PIXI.Texture[];
    protected tailTextures: PIXI.Texture[];

    protected autoMove: boolean;
    protected spacebarPressed: boolean;

    onInit() {
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute(ATTR_FACTORY);

        this.timePassed = 0;
        this.autoMove = false;
        this.spacebarPressed = true;

        this.loadTextures();
    }

    move() {
        let currentNode = this.model.snakeSprites.firstNode;
        let moveTail = true;
        this.addSprite();

        switch (this.model.snakeDirection) {
            case DIRECTION.Down:
                currentNode.element.position.y++;
                if (currentNode.element.position.y == GAME_OFFSET + ROWS_NUM) currentNode.element.position.y = GAME_OFFSET;
                break;
            case DIRECTION.Left:
                currentNode.element.position.x--;
                if (currentNode.element.position.x == -1) currentNode.element.position.x = COLUMNS_NUM - 1;
                break;
            case DIRECTION.Right:
                currentNode.element.position.x++;
                if (currentNode.element.position.x == COLUMNS_NUM) currentNode.element.position.x = 0;
                break;
            case DIRECTION.Up:
                if (currentNode.element.position.y == GAME_OFFSET) currentNode.element.position.y = GAME_OFFSET + ROWS_NUM;
                currentNode.element.position.y--;
                break;
        }

        let collisionTile = this.model.getTile(new Vec2(currentNode.element.x, currentNode.element.y));
        switch (collisionTile) {
            case TileType.Snake:
                let tail = this.model.snakeSprites.last();
                if (tail.x != currentNode.element.x || tail.y != currentNode.element.y) {
                    this.sendMessage(MSG_OBJECT_HIT, HitType.Snake);
                }
                break;
            case TileType.Apple:
                moveTail = false;
                this.sendMessage(MSG_OBJECT_HIT, HitType.Apple);
                break;
            case TileType.Wall:
                this.sendMessage(MSG_OBJECT_HIT, HitType.Wall);
                break;
            default:
                break;
        }

        if (moveTail) this.moveTail();

        currentNode.element.texture = this.headTextures[this.model.snakeDirection];
        this.model.addTile(new Vec2(currentNode.element.x, currentNode.element.y), TileType.Snake);

        while (currentNode != null) {
            currentNode = currentNode.next;
        }
    }

    addSprite() {
        let builder = new PIXIObjectBuilder(this.scene);

        let sprite: PIXICmp.Sprite;
        let currentNode = this.model.snakeSprites.firstNode;
        let previousNode = currentNode.next;
        //pridat sprite
        if (previousNode.element.x == currentNode.element.x + 1 || (previousNode.element.x == 0 && currentNode.element.x == COLUMNS_NUM - 1)) {
            //vpravo
            switch (this.model.snakeDirection) {
                case DIRECTION.Down:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_DOWN_RIGHT)))
                    break;
                case DIRECTION.Left:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_LEFT_RIGHT)))
                    break;
                case DIRECTION.Right:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_LEFT_RIGHT)))
                    break;
                case DIRECTION.Up:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_UP_RIGHT)))
                    break;
            }
        }
        else if (previousNode.element.x == currentNode.element.x - 1 || (previousNode.element.x == COLUMNS_NUM - 1 && currentNode.element.x == 0)) {
            //vlevo
            switch (this.model.snakeDirection) {
                case DIRECTION.Down:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_DOWN_LEFT)))
                    break;
                case DIRECTION.Left:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_LEFT_RIGHT)))
                    break;
                case DIRECTION.Right:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_LEFT_RIGHT)))
                    break;
                case DIRECTION.Up:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_UP_LEFT)))
                    break;
            }
        }
        else if (previousNode.element.y == currentNode.element.y + 1 || (previousNode.element.y == GAME_OFFSET && currentNode.element.y == GAME_OFFSET + ROWS_NUM - 1)) {
            //dole
            switch (this.model.snakeDirection) {
                case DIRECTION.Down:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_UP_DOWN)))
                    break;
                case DIRECTION.Left:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_DOWN_LEFT)))
                    break;
                case DIRECTION.Right:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_DOWN_RIGHT)))
                    break;
                case DIRECTION.Up:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_UP_DOWN)))
                    break;
            }
        }
        else if (previousNode.element.y == currentNode.element.y - 1 || previousNode.element.y == GAME_OFFSET + ROWS_NUM - 1) {
            //nahore
            switch (this.model.snakeDirection) {
                case DIRECTION.Down:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_UP_DOWN)))
                    break;
                case DIRECTION.Left:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_UP_LEFT)))
                    break;
                case DIRECTION.Right:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_UP_RIGHT)))
                    break;
                case DIRECTION.Up:
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.factory.createTexture(this.model.getSpriteInfo(TAG_SNAKE_UP_DOWN)))
                    break;
            }
        }
        this.model.snakeSprites.add(sprite, this.model.snakeSprites.indexOf(previousNode.element));

        builder
            .scale(Factory.globalScale)
            .globalPos(currentNode.element.x, currentNode.element.y)
            .build(sprite, this.scene.stage);
    }

    moveTail() {
        let tail = this.model.snakeSprites.last();
        let indexTail = this.model.snakeSprites.indexOf(tail);

        let previousNode = this.model.snakeSprites.elementAtIndex(indexTail - 1);
        let previousPreviousNode = this.model.snakeSprites.elementAtIndex(indexTail - 2);

        if (previousNode.x == previousPreviousNode.x - 1 || (previousNode.x == COLUMNS_NUM - 1 && previousPreviousNode.x == 0)) {
            previousNode.texture = this.tailTextures[DIRECTION.Left];
        }
        else if (previousNode.x == previousPreviousNode.x + 1 || (previousNode.x == 0 && previousPreviousNode.x == COLUMNS_NUM - 1)) {
            previousNode.texture = this.tailTextures[DIRECTION.Right];
        }
        else if (previousNode.y == previousPreviousNode.y - 1 || (previousNode.y == GAME_OFFSET + ROWS_NUM - 1 && previousPreviousNode.y == GAME_OFFSET)) {
            previousNode.texture = this.tailTextures[DIRECTION.Up];
        }
        else if (previousNode.y == previousPreviousNode.y + 1 || (previousNode.y == GAME_OFFSET && previousPreviousNode.y == GAME_OFFSET + ROWS_NUM - 1)) {
            previousNode.texture = this.tailTextures[DIRECTION.Down];
        }

        this.model.removeTile(new Vec2(tail.x, tail.y));
        this.model.snakeSprites.removeElementAtIndex(indexTail);
        tail.remove();
    }

    loadTextures() {
        this.headTextures = [];
        this.headTextures[DIRECTION.Down] = this.factory.createTexture(this.model.getSpriteInfo(TAG_HEAD_DOWN));
        this.headTextures[DIRECTION.Right] = this.factory.createTexture(this.model.getSpriteInfo(TAG_HEAD_RIGHT));
        this.headTextures[DIRECTION.Left] = this.factory.createTexture(this.model.getSpriteInfo(TAG_HEAD_LEFT));
        this.headTextures[DIRECTION.Up] = this.factory.createTexture(this.model.getSpriteInfo(TAG_HEAD_UP));

        this.tailTextures = [];
        this.tailTextures[DIRECTION.Down] = this.factory.createTexture(this.model.getSpriteInfo(TAG_TAIL_DOWN));
        this.tailTextures[DIRECTION.Right] = this.factory.createTexture(this.model.getSpriteInfo(TAG_TAIL_RIGHT));
        this.tailTextures[DIRECTION.Left] = this.factory.createTexture(this.model.getSpriteInfo(TAG_TAIL_LEFT));
        this.tailTextures[DIRECTION.Up] = this.factory.createTexture(this.model.getSpriteInfo(TAG_TAIL_UP));
    }
}

/**
 * Keyboard component for snake
 */
export class SnakeInputController extends SnakeController {
    onUpdate(delta: number, absolute: number) {
        // get a global component
        let cmp = this.scene.stage.findComponentByClass(KeyInputComponent.name);
        let cmpKey = <KeyInputComponent><any>cmp;

        if (cmpKey.isKeyPressed(KEY_SPACEBAR)) {
            if (!this.spacebarPressed) {
                this.autoMove = !this.autoMove;
                this.spacebarPressed = true;
            }
        }
        else {
            this.spacebarPressed = false;
        }

        let currentNode = this.model.snakeSprites.firstNode;
        let previousNode = currentNode.next;
        if (!this.autoMove) {
            //KEYBOARD MOVE

            if (cmpKey.isKeyPressed(KEY_LEFT) && previousNode.element.x != currentNode.element.x - 1) {
                this.model.snakeDirection = DIRECTION.Left;
            }

            if (cmpKey.isKeyPressed(KEY_RIGHT) && previousNode.element.x != currentNode.element.x + 1) {
                this.model.snakeDirection = DIRECTION.Right;
            }

            if (cmpKey.isKeyPressed(KEY_UP) && previousNode.element.y != currentNode.element.y - 1) {
                this.model.snakeDirection = DIRECTION.Up;
            }
            if (cmpKey.isKeyPressed(KEY_DOWN) && previousNode.element.y != currentNode.element.y + 1) {
                this.model.snakeDirection = DIRECTION.Down;
            }
        }
        else {
            //PATHFINDER MOVE
            let context = new PathFinderContext();

            let found = new BreadthFirstSearch().search(this.model.tiles, new Vec2(currentNode.element.x, currentNode.element.y), new Vec2(this.model.apple.x, this.model.apple.y), context,
                (vec: Vec2) => vec.y * COLUMNS_NUM + vec.x);

            if (found) {
                //NASTAV SMER
                let from = context.pathFound[0];
                let to = context.pathFound[1];

                if (to) {
                    if (from.x < to.x) this.model.snakeDirection = DIRECTION.Right;
                    if (from.x > to.x) this.model.snakeDirection = DIRECTION.Left;
                    if (from.y < to.y) this.model.snakeDirection = DIRECTION.Down;
                    if (from.y > to.y) this.model.snakeDirection = DIRECTION.Up;
                }
            }

        }

        this.timePassed += delta;
        if (this.timePassed > 500 / this.model.gameSpeedMultiplier) {
            this.move();
            this.timePassed = 0;
        }
    }
}