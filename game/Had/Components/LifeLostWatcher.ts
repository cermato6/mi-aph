import { Model } from '../Model';
import { ATTR_MODEL, MSG_LIFELOST_COLLISION, MSG_COMMAND_FINISH_LEVEL, MSG_COMMAND_RESPAWN } from '../Constants';
import Component from '../../../ts/engine/Component';
import Msg from '../../../ts/engine/Msg';

/**
 * Watcher for lost lives
 */
export class LifeLostWatcher extends Component {

    private model: Model;

    onInit() {
        this.subscribe(MSG_LIFELOST_COLLISION);
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_LIFELOST_COLLISION) {
            if (this.model.currentLives > 1) {
                this.model.currentLives--;
                this.sendMessage(MSG_COMMAND_RESPAWN);
            }
            else {
                this.sendMessage(MSG_COMMAND_FINISH_LEVEL);
            }
        }
    }
}