import { PathFinderContext } from "../../ts/utils/Pathfinding";
import { TileType } from "./Constants";
import Vec2 from "../../ts/utils/Vec2";
import PriorityQueue from "../../ts/utils/datastruct/PriorityQueue";
import { COLUMNS_NUM, GAME_OFFSET, ROWS_NUM } from "./Model";
import Queue from "../../ts/utils/datastruct/Queue";

class Pair<A, B>{
    first: A;
    second: B;
    constructor(first: A, second: B) {
        this.first = first;
        this.second = second;
    }
}

export abstract class PathFinder {

    abstract search(grid: Map<number, TileType>, start: Vec2, goal: Vec2, outputCtx: PathFinderContext, indexMapper: (Vec2) => number): boolean;

    protected calcPathFromSteps(start: Vec2, goal: Vec2, steps: Map<number, Vec2>, indexMapper: (Vec2) => number): Array<Vec2> {
        let current = goal;
        let output = new Array<Vec2>();
        output.push(current);
        while (!current.equals(start)) {
            current = steps.get(indexMapper(current));
            output.push(current);
        }
        // reverse path so the starting position will be at the first place
        output = output.reverse();
        return output;
    }
}


export class BreadthFirstSearch extends PathFinder {
    search(grid: Map<number, TileType>, start: Vec2, goal: Vec2, outputCtx: PathFinderContext, indexMapper: (Vec2) => number): boolean {
        let frontier = new Queue<Vec2>();
        frontier.add(start);

        outputCtx.cameFrom.set(indexMapper(start), start);

        while (!frontier.isEmpty()) {
            let current = frontier.peek();
            outputCtx.visited.add(indexMapper(current));

            frontier.dequeue();

            if (current.equals(goal)) {
                // the goal was achieved
                outputCtx.pathFound = this.calcPathFromSteps(start, goal, outputCtx.cameFrom, indexMapper);
                return true;
            }

            // get neighbors of the current grid block
            let neighbors = new Array<Vec2>();
            let LTile, RTile, UTile, DTile;
            if(current.x > 0) LTile = grid.get(current.x + COLUMNS_NUM * current.y - 1);
            else LTile = TileType.Wall;

            if(current.x < COLUMNS_NUM - 1) RTile = grid.get(current.x + COLUMNS_NUM * current.y + 1);
            else RTile = TileType.Wall;

            if(current.y > GAME_OFFSET) UTile = grid.get(current.x + COLUMNS_NUM * (current.y - 1));
            else UTile = TileType.Wall;

            if(current.y < GAME_OFFSET + ROWS_NUM - 1) DTile = grid.get(current.x + COLUMNS_NUM * (current.y + 1));
            else DTile = TileType.Wall;       

            if(!LTile || LTile == TileType.Apple){
                neighbors.push(new Vec2(current.x - 1, current.y))
            }
            if(!RTile || RTile == TileType.Apple){
                neighbors.push(new Vec2(current.x + 1, current.y))
            }
            if(!UTile || UTile == TileType.Apple){
                neighbors.push(new Vec2(current.x, current.y - 1))
            }
            if(!DTile || DTile == TileType.Apple){
                neighbors.push(new Vec2(current.x, current.y + 1))
            }

            for (let next of neighbors) {
                if (!outputCtx.cameFrom.has(indexMapper(next))) {
                    frontier.enqueue(next);
                    outputCtx.cameFrom.set(indexMapper(next), current);
                }
            }
        }
        return false;
    }
}