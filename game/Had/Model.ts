import Vec2 from '../../ts/utils/Vec2';
import LinkedList from '../../ts/utils/datastruct/LinkedList'
import { TileType, DIRECTION, MSG_COMMAND_RESPAWN, MSG_COMMAND_FINISH_LEVEL, MSG_GAME_COMPLETED, MSG_COMMAND_NEWGAME } from './Constants';
import { PIXICmp } from '../../ts/engine/PIXIObject';

// number of columns of each level
export const COLUMNS_NUM = 32;
// number of rows of each level
export const ROWS_NUM = 20;
// game offset
export const GAME_OFFSET = 2;

/**
 * Entity that stores metadata about each sprite as loaded from JSON file
 */
export class SpriteInfo {

    constructor(name: string, offsetX: number, offsetY: number, width: number, height: number, frames: number) {
        this.name = name;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.width = width;
        this.height = height;
        this.frames = frames;
    }

    name: string;
    offsetX: number;
    offsetY: number;
    width: number;
    height: number;
    frames: number;
}

export class Model {
    // tiles mapped by their indices
    tiles = new Map<number, TileType>();

    // snake as linkedlist, where first is the head
    snakeSprites = new LinkedList<PIXICmp.Sprite>();

    // apple sprite
    apple: Vec2;
    appleSprite: PIXICmp.Sprite;


    // metadata of all sprites as loaded from JSON file
    sprites: Array<SpriteInfo>;
    // 2D arrays of levels as loaded from JSON file
    levels: Array<Array<Array<number>>>;

    maxLives: Array<number>;
    maxLevel: number;
    // mapped number of level to indices
    initSnakePosition: Map<number, number>;


    //=============== dynamic attributes (changed as the game is played)
    //0.5 second / gameSpeedMultiplier
    gameSpeedMultiplier = 1;
    currentLevel = 0;
    currentScore = 0;
    currentLives = 0;
    snakeDirection = DIRECTION.Right;

    /**
     * Loads model from a JSON structure
     */
    loadModel(data: any) {
        this.sprites = new Array<SpriteInfo>();
        this.snakeSprites = new LinkedList<PIXICmp.Sprite>();

        for (let spr of data.sprites) {
            this.sprites.push(new SpriteInfo(spr.name, spr.offset_px_x, spr.offset_px_y, spr.sprite_width, spr.sprite_height, spr.frames));
        }

        this.levels = new Array();

        for (let level of data.levels_maps) {
            this.levels.push(level);
        }

        this.maxLevel = this.levels.length;

        this.maxLives = new Array();
        for (let lives of data.levels_parameters.lives) {
            this.maxLives.push(lives);
        }
    }

    /**
     * Initializes the current level
     */
    initLevel(cmd: string) {
        this.tiles.clear();
        this.snakeSprites.clear();
        this.initSnakePosition = new Map<number, number>();

        switch (cmd) {
            case MSG_COMMAND_FINISH_LEVEL:
                this.currentLives = this.maxLives[this.currentLevel - 1];
                break;
            case MSG_COMMAND_NEWGAME:
                this.currentScore = 0;
                this.currentLives = this.maxLives[this.currentLevel - 1];
                break;
        }
        this.gameSpeedMultiplier = 1;

        this.loadTiles();
    }

    getSpriteInfo(name: string): SpriteInfo {
        for (let spr of this.sprites) {
            if (spr.name == name) return spr;
        }
        return null;
    }

    getTile(position: Vec2): TileType {
        let index = position.y * COLUMNS_NUM + position.x;
        return this.tiles.get(index);
    }

    addTile(position: Vec2, type: TileType) {
        let index = position.y * COLUMNS_NUM + position.x;
        this.tiles.set(index, type);
    }
    removeTile(position: Vec2) {
        let index = position.y * COLUMNS_NUM + position.x;
        this.tiles.delete(index);
    }

    getRandomEmptyTile(): Vec2 {
        if (this.tiles.size == ROWS_NUM * COLUMNS_NUM) return undefined;

        let position = new Vec2(0, 0);
        while (true) {
            position.x = Math.floor(Math.random() * COLUMNS_NUM);
            position.y = Math.floor(Math.random() * (ROWS_NUM - GAME_OFFSET + 1) + GAME_OFFSET);
            if (!this.getTile(position)) break;
        }

        return position;
    }

    /**
     * Fills map of bricks from an array the level is represented by
     */
    protected loadTiles() {
        let snakeOffset = TileType.Snake;
        for (let row = 0; row < this.levels[this.currentLevel - 1].length; row++) {
            for (let col = 0; col < COLUMNS_NUM; col++) {

                let tileIndex = this.levels[this.currentLevel - 1][row][col];

                if (tileIndex != TileType.None) {
                    let index = row * COLUMNS_NUM + col;
                    if (tileIndex == TileType.Wall) {
                        this.tiles.set(index + GAME_OFFSET * COLUMNS_NUM, TileType.Wall);
                    }
                    else {
                        this.initSnakePosition.set(tileIndex - snakeOffset, index);
                        this.tiles.set(index + GAME_OFFSET * COLUMNS_NUM, TileType.Snake);
                    }
                }
            }
        }
    }
}
