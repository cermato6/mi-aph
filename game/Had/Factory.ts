import { GameComponent } from './Components/GameComponent';
import Scene from '../../ts/engine/Scene';
import { Model, SpriteInfo, COLUMNS_NUM, GAME_OFFSET } from './Model';
import { SoundComponent } from './Components/SoundComponent';
import PIXIObjectBuilder from '../../ts/engine/PIXIObjectBuilder';
import { IntroComponent } from './Components/IntroComponent';
import { PIXICmp } from '../../ts/engine/PIXIObject';
import { KeyInputComponent } from '../../ts/components/KeyInputComponent';
import { ATTR_FACTORY, ATTR_MODEL, TAG_INTRO, TEXTURE_HAD, TEXTURE_BACKGROUND, TAG_BACKGROUND, TAG_HEAD_RIGHT, TAG_SNAKE, TAG_TAIL_LEFT, DIRECTION, TAG_HEAD_DOWN, TAG_HEAD_LEFT, TAG_HEAD_UP, TAG_SNAKE_LEFT_RIGHT, TAG_SNAKE_UP_DOWN, TAG_TAIL_RIGHT, TAG_TAIL_DOWN, TAG_TAIL_UP, TAG_SNAKE_UP_LEFT, TAG_SNAKE_DOWN_LEFT, TAG_SNAKE_UP_RIGHT, TAG_SNAKE_DOWN_RIGHT, TileType, TAG_WALL, TAG_APPLE, TAG_SCORE, TAG_LIFE, TEXTURE_INTRO, TAG_STATUS } from './Constants';
import { Container } from 'pixi.js';
import Vec2 from '../../ts/utils/Vec2';
import { SnakeInputController } from './Components/SnakeComponent';
import { SnakeCollisionResolver } from './Components/SnakeCollisionResolver';
import { AppleSpawner } from './Components/AppleSpawner';
import { AppleDisplayComponent } from './Components/AppleDisplayComponent';
import { AppleEatenWatcher } from './Components/AppleEatenWatcher';
import { LifeLostWatcher } from './Components/LifeLostWatcher';
import { LifeDisplayComponent } from './Components/LifeDisplayComponent';
import { StatusComponent } from './Components/StatusComponent';
import { GameSpeedWatcher } from './Components/GameSpeedWatcher';

export class Factory {

    static globalScale = 1;

    initializeLevel(scene: Scene, model: Model, cmd: string) {
        // scale the scene
        if (model.currentLevel == 0) {
            this.addIntro(scene, model);
        } else {
            model.initLevel(cmd);

            this.addBackground(scene, model);

            this.addWalls(scene, model);
            this.addSnake(scene, model);
            this.addApple(scene, model);
            this.addScore(scene, model);
            this.addLives(scene, model);
            this.addStatus(scene, model);

            scene.addGlobalComponent(new SoundComponent());
            scene.addGlobalComponent(new KeyInputComponent());
            scene.addGlobalComponent(new GameComponent());
            scene.addGlobalComponent(new AppleSpawner());
            scene.addGlobalComponent(new AppleEatenWatcher());
            scene.addGlobalComponent(new GameSpeedWatcher())
            scene.addGlobalComponent(new LifeLostWatcher());
        }
    }

    addIntro(scene: Scene, model: Model) {
        let builder = new PIXIObjectBuilder(scene);

        // stage components
        builder
            .withComponent(new SoundComponent())
            .withComponent(new IntroComponent())
            .build(scene.stage)

        // title
        builder
            .globalPos(0, GAME_OFFSET)
            .scale(Factory.globalScale)
            .build(new PIXICmp.Sprite(TAG_INTRO, this.createTextureFromImage(TEXTURE_INTRO)), scene.stage);
    }
    addStatus(scene: Scene, model: Model) {
        let status = new PIXICmp.Text(TAG_STATUS);
        status.style = new PIXI.TextStyle({
            fontFamily: "Comfont",
            fill: "0xFFFFFF"
        });

        new PIXIObjectBuilder(scene)
        .scale(Factory.globalScale)
        .localPos(12, GAME_OFFSET + 9)
        .withComponent(new StatusComponent())
        .build(status, scene.stage);
    }

    addBackground(scene: Scene, model: Model) {
        let builder = new PIXIObjectBuilder(scene);
        //background
        builder
            .globalPos(0, GAME_OFFSET)
            .scale(Factory.globalScale)
            .build(new PIXICmp.Sprite(TAG_BACKGROUND, this.createTextureFromImage(TEXTURE_BACKGROUND)), scene.stage);
    }

    addScore(scene: Scene, model: Model) {
        let builder = new PIXIObjectBuilder(scene);
        let text = new PIXICmp.Text(TAG_SCORE, `SCORE: ${model.currentScore}`)
        text.style.fill = 0xffffff;

        builder
            .scale(Factory.globalScale)
            .globalPos(0, 0)
            .withComponent(new AppleDisplayComponent())
            .build(text, scene.stage)
    }

    addLives(scene: Scene, model: Model) {
        // for each life, create a small icon
        for (let i = 1; i <= model.currentLives; i++) {
            let sprite = new PIXICmp.Sprite(TAG_LIFE + "_" + i, this.createTexture(model.getSpriteInfo(TAG_LIFE)));
            scene.stage.getPixiObj().addChild(sprite);
            sprite.scale.set(Factory.globalScale, Factory.globalScale);

            // place them to the bottom left
            sprite.position.x = i - 1;
            sprite.position.y = 23;
        }

        scene.stage.addComponent(new LifeDisplayComponent());
    }

    addWalls(scene: Scene, model: Model) {
        let builder = new PIXIObjectBuilder(scene);
        for (let [key, value] of model.tiles) {
            if (value == TileType.Wall) {
                builder
                    .scale(Factory.globalScale)
                    .globalPos(key - Math.floor(key / COLUMNS_NUM) * COLUMNS_NUM, Math.floor(key / COLUMNS_NUM))
                    .build(new PIXICmp.Sprite(TAG_WALL, this.createTexture(model.getSpriteInfo(TAG_WALL))), scene.stage);
            }
        }
    }

    addApple(scene: Scene, model: Model) {
        let builder = new PIXIObjectBuilder(scene);

        let sprite = new PIXICmp.Sprite(TAG_APPLE, this.createTexture(model.getSpriteInfo(TAG_APPLE)));

        let position = model.getRandomEmptyTile();
        if (position) {
            builder
                .scale(Factory.globalScale)
                .globalPos(position.x, position.y)
                .build(sprite, scene.stage);
        }
        model.apple = position;
        model.appleSprite = sprite;
        model.tiles.set(position.x + position.y * COLUMNS_NUM, TileType.Apple);
    }

    addSnake(scene: Scene, model: Model) {
        let builder = new PIXIObjectBuilder(scene);

        for (let i = 0; i < model.initSnakePosition.size; i++) {
            let sprite: PIXICmp.Sprite;
            if (i == 0) {
                //HEAD
                let current = model.initSnakePosition.get(i);
                let next = model.initSnakePosition.get(i + 1);
                if (next) {
                    if (next == current - 1) {
                        //vpravo
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_HEAD_RIGHT)));
                        model.snakeDirection = DIRECTION.Right;
                    }
                    else if (next == current + 1) {
                        //vlevo
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_HEAD_LEFT)));
                        model.snakeDirection = DIRECTION.Left;
                    }
                    else if (next == current - COLUMNS_NUM) {
                        //dole
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_HEAD_DOWN)));
                        model.snakeDirection = DIRECTION.Down;
                    }
                    else if (next == current + COLUMNS_NUM) {
                        //nahore
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_HEAD_UP)));
                        model.snakeDirection = DIRECTION.Up;
                    }
                    else {
                        throw ("BAD SNAKE")
                    }
                }
                else {
                    throw ("SNAKE LENGTH AT LEAST 2");
                }

                builder
                    .scale(Factory.globalScale)
                    .globalPos(current - Math.floor(current / COLUMNS_NUM) * COLUMNS_NUM, Math.floor(current / COLUMNS_NUM) + GAME_OFFSET)
                    .withComponent(new SnakeInputController())
                    .withComponent(new SnakeCollisionResolver())
                    .build(sprite, scene.stage);
            }
            else if (i != model.initSnakePosition.size - 1) {
                //body
                let current = model.initSnakePosition.get(i);
                let previous = model.initSnakePosition.get(i - 1);
                let next = model.initSnakePosition.get(i + 1);
                if (previous == current - 1) {
                    //vlevo
                    if (next == current - COLUMNS_NUM) {
                        //nahore
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_UP_LEFT)));
                    }
                    else if (next == current + COLUMNS_NUM) {
                        //dole
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_DOWN_LEFT)));
                    }
                    else if (next == current + 1) {
                        //vpravo
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_LEFT_RIGHT)));
                    }
                    else {
                        throw ("BAD SNAKE");
                    }
                }
                else if (previous == current + 1)
                    //vpravo
                    if (next == current - COLUMNS_NUM) {
                        //nahore
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_UP_RIGHT)));
                    }
                    else if (next == current + COLUMNS_NUM) {
                        //dole
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_DOWN_RIGHT)));
                    }
                    else if (next == current - 1) {
                        //vlevo
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_LEFT_RIGHT)));
                    }
                    else {
                        throw ("BAD SNAKE");
                    }

                else if (previous == current - COLUMNS_NUM) {
                    //nahore
                    if (next == current + COLUMNS_NUM) {
                        //dole
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_UP_DOWN)));
                    }
                    else if (next == current + 1) {
                        //vpravo
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_UP_RIGHT)));
                    }
                    else if (next == current - 1) {
                        //vlevo
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_UP_LEFT)));
                    }
                    else {
                        throw ("BAD SNAKE");
                    }
                }
                else if (previous == current + COLUMNS_NUM) {
                    //dole
                    if (next == current - COLUMNS_NUM) {
                        //nahore
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_UP_DOWN)));
                    }
                    else if (next == current + 1) {
                        //vpravo
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_DOWN_RIGHT)));
                    }
                    else if (next == current - 1) {
                        //vlevo
                        sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_SNAKE_DOWN_LEFT)));
                    }
                    else {
                        throw ("BAD SNAKE");
                    }
                }
                else {
                    throw ("BAD SNAKE")
                }
                builder
                    .scale(Factory.globalScale)
                    .globalPos(current - Math.floor(current / COLUMNS_NUM) * COLUMNS_NUM, Math.floor(current / COLUMNS_NUM) + GAME_OFFSET)
                    .build(sprite, scene.stage);
            }
            else {
                //TAIL
                let current = model.initSnakePosition.get(i);
                let previous = model.initSnakePosition.get(i - 1);
                if (previous == current - 1) {
                    //vpravo
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_TAIL_RIGHT)));
                }
                else if (previous == current + 1) {
                    //vlevo
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_TAIL_LEFT)));
                }
                else if (previous == current - COLUMNS_NUM) {
                    //dole
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_TAIL_DOWN)));
                }
                else if (previous == current + COLUMNS_NUM) {
                    //nahore
                    sprite = new PIXICmp.Sprite(TAG_SNAKE, this.createTexture(model.getSpriteInfo(TAG_TAIL_UP)));
                }
                else {
                    throw ("BAD SNAKE")
                }
                builder
                    .scale(Factory.globalScale)
                    .globalPos(current - Math.floor(current / COLUMNS_NUM) * COLUMNS_NUM, Math.floor(current / COLUMNS_NUM) + GAME_OFFSET)
                    .build(sprite, scene.stage);
            }
            model.snakeSprites.add(sprite);
        }
    }

    resetGame(scene: Scene, model: Model, cmd: string) {
        scene.clearScene();
        scene.addGlobalAttribute(ATTR_FACTORY, this);
        scene.addGlobalAttribute(ATTR_MODEL, model);
        this.initializeLevel(scene, model, cmd);
    }

    // loads texture from SpriteInfo entity
    public createTexture(spriteInfo: SpriteInfo, index: number = 0): PIXI.Texture {
        let texture = PIXI.Texture.fromImage(TEXTURE_HAD);
        texture = texture.clone();
        texture.frame = new PIXI.Rectangle(spriteInfo.offsetX + spriteInfo.width * index, spriteInfo.offsetY, spriteInfo.width, spriteInfo.height);
        return texture;
    }
    public createTextureFromImage(image: string): PIXI.Texture {
        let texture = PIXI.Texture.fromImage(image);
        texture = texture.clone();
        return texture;
    }
}