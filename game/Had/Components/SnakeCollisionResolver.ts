import { ATTR_MODEL, MSG_OBJECT_HIT, HitType, MSG_APPLE_COLLISON, MSG_LIFELOST_COLLISION } from '../Constants';
import Component from '../../../ts/engine/Component';
import { Model } from '../Model';
import Msg from '../../../ts/engine/Msg';

/**
 * Component that handles collision with things
 */
export class SnakeCollisionResolver extends Component {
    private model: Model;

    onInit() {
        this.subscribe(MSG_OBJECT_HIT);
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_OBJECT_HIT) {
            let hitType: HitType = msg.data;
            switch (hitType) {
                case HitType.Apple:
                    this.sendMessage(MSG_APPLE_COLLISON);
                    break;
                case HitType.Snake:
                case HitType.Wall:
                    this.sendMessage(MSG_LIFELOST_COLLISION);
                    break;
                default:
                    break;
            }
        }
    }
}